## 实现功能：
1、虚拟主机站点（支持正式域名和临时域名）
2、ftp账户
3、mysql账户
4、站点文件权限（根据程序需要赋予777权限）

=======================================

## 原理：
给apache安装了apache2-mpm-itk组件，该组件可以让虚拟主机管理软件使用root权限和sh脚本,
而生成出来的虚拟主机只拥有普通权限，php程序采用了CI程序开发。

优点：超轻量级，非侵入式，可以在编译方式安装lamp的基础上使用，过程脚本全透明。

缺点：安全性还待考虑。

## 使用说明：
基本环境是lamp，并安装了vsftpd（根据系统用户登录），默认的虚拟站点目录是/www/web/，默认ftp管理员是ftp_super，归属用户组fptall，需要开启防火墙端口大于等于10300，供临时站点使用

部署的时候需要修改账户或密码的地方(在php和脚本文件中对应着有)：
1. ftp超级管理员和组
2. mysql的root管理员帐户密码

### 一、先安装apache2-mpm-itk组件，教程：https://www.centos.bz/2011/11/apache-virtualhost-different-user-with-apache2-mpm-itk/

如果已经编译安装的话，先卸载：

1. 备份httpd的配置 conf文件夹，方便和新配置后的对比修改

2.备份apache目录下的moudles下面的so文件，比如mod_fastcgi.so和libphp5.so(如果没有这个，就无法支持php_admin_value)，如果忘记备份只能重新安装了

3.备份好/usr/local/php/lib/php/extensions/no-debug-non-zts-20060613/（根据php版本不同而不同） 下面有扩展编译的so文件：gd、mbstring、eaccelerator

4.最好备份/etc/php.ini文件，防止php编译重装

5.删除/usr/local/apache

6.删除/tmp下面的编译过的文件夹并重新解压


### 二、ftpadd、ftpdel、siteadd、sitedel 脚本文件放到/usr/local/bin/下面，并赋予执行权限 chown +x

### 三、在apapche中新建一个站点，并赋予root站点运行权限

下面是例子：

vi /etc/httpd/conf/extra/httpd-vhosts.conf

内容如下：

	<Directory /www/web>
	    AllowOverride All
	</Directory>
	NameVirtualHost *:80
	<VirtualHost *:80>
	  <Location />
	    Order allow,deny
	    Allow from all
	    Satisfy all
	  </Location>
	</VirtualHost>
	<VirtualHost *:80>
	  ServerAlias host.xxx.com
	  DocumentRoot /www/web/host.xxx.com
	  php_admin_value open_basedir /www/web/host.xxx.com/:/tmp/
	  <IfModule mpm_itk_module>
	    AssignUserId root root
	  </IfModule>
	</VirtualHost>


### 四、有两个配置虚拟主机配置文件，分别是正式域名和临时域名的

/etc/httpd/conf/extra/dc-site.conf

/etc/httpd/conf/extra/dc-temp.conf

每开一个虚拟主机，就会去修改配置文件，并自动重新载入配置文件

### 五、ftp开头的bash脚本含有超级ftp用户ftp_super设置，可以任意修改，同时在D_shell.php中也有配置，需要一起修改

### 六、开mysql用户是在D_shell.php中配置和完成，并没有利用脚本

### 七、注意删除虚拟主机站点，会删除站点文件、ftp用户、mysql用户和数据库

### 八、界面

1、列表

![list](http://git.oschina.net/uploads/images/2014/1015/182640_c84e84ac_107323.jpeg)
2、添加

![add](http://git.oschina.net/uploads/images/2014/1015/182821_de2ba096_107323.jpeg)

