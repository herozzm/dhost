<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	
	/**
     * 构造函数
     *
     * @access  public
     * @return  void
     */
    public function __construct()
    {
        parent::__construct();
		$this->load->library('session');
    }
	
	
	 /**
     * 登录入口
     *
     * @access  public
     * @return  void
     */
	public function index()
	{
		$this->load->view('home');
	}
	
	/**
     * 验证登录
     *
     * @access  public
     * @return  void
     */
	public function login()
	{
		$uname = $this->input->post('uname');
		$pwd   = $this->input->post('pwd');
		
		if(empty($uname) || empty($pwd))
		{
			redirect('home');
		} else {
			$this->db->select('u_id')->from('admin')->where(array('uname'=>$uname, 'pwd'=>$pwd));
			$query = $this->db->get();
			if($query->num_rows()>0)
			{
				$user_info = $query->row_array();	
				$this->session->set_userdata($user_info);
				redirect('main');
			} else {
				redirect('home');
			}
			
		}
	}
	
	/*
	 * 修改密码
	 * @access  public
     * @return  void
	 */
	public function passwd($act='')
	{
		$this->load->library('session');
        $this->load->helper('islogin');
		islogin();
		
		$this->load->view('passwd', $data);
	}
	
	/**
     * 退出登录
     *
     * @access  public
     * @return  void
     */
	public function out()
	{
		$this->session->unset_userdata($this->session->all_userdata());
		redirect('home');
	}
	
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */