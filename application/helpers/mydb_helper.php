<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 返回首行或者首行第一列
 * @param   string           $sql 适用自定义的sql语句 
 *
 * @return  array or sring   $rt  返回首行，如果只有一列就直接返回第一列
 */
if ( ! function_exists('get_one'))
{
	function get_one($sql) {
		$CI =& get_instance();
		$query = $CI->db->query($sql);
		$rt = $query->row_array();		
		return is_array($rt) && count($rt)==1 ? reset($rt) : $rt;
	}
}

/**
 * 返回所有行
 * @param   string           $sql 适用自定义的sql语句 
 *
 * @return  array or sring   $rt  返回所有行
 */
if ( ! function_exists('get_all'))
{
	function get_all($sql) {
		$CI =& get_instance();
		$query = $CI->db->query($sql);
		$rt = $query->result_array();		
		return $rt;
	}
}