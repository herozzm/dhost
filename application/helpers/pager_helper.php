<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 生成翻页码
 * @param   int          $total       总记录
 * @param   int          $page        当前页
 * @param   int          $num         每页显示几条
 * @param   string       $url         翻页网址
 * @param   int          $show_num    显示几个页码+1
 *
 * @return  string       $page_con    翻页内容
 */
if ( ! function_exists('pager'))
{
	function pager($url='', $total, $page='1', $num=15, $show_num=10)
	{	
	    $CI =& get_instance();		
		
		$pages     = ceil($total/$num); //总页数
		
		//$page_con = '<div class="setnum">总计 '.$total.' 个记录</div>';
		
		//取将当前页面至于中间页码，只显示10个页码
		$m = ceil($show_num/2);
		
		$pages_start = $page-$m>0 ? $page-$m : 1;
		$pages_end   = $pages_start+$show_num > $pages ? $pages :$pages_start+$show_num;
		$pages_start = $pages_end-$pages_start<$show_num ? $pages_end-$show_num : $pages_start;
		$pages_start = $pages_start<1 ? 1 : $pages_start;
		$page_con = '';
		for($i=$pages_start; $i<=$pages_end; $i++)
		{
			$page_con .= $i==$page ? "<span>$i</span>" : '<a href="'.site_url($url.'page='.$i).'">'.$i.'</a>';
		}
		
		//左边部分
		$left ='';
		//加入上一页箭头
		if($page>1)
		{
			$left = '<a href="'.site_url($url.'page='.($page-1)).'">&laquo;</a>';
		}
		//加入第一页
		if($pages_start>1)
		{
			$left .= '<a href="'.site_url($url.'page=1').'">1</a>';
		}
		$page_con = $left.$page_con;	
		
		//右边部分
		$right ='';	
		//加入最后一页
		if($pages_end < $pages-1)
		{
			$right = '<a href="'.site_url($url.'page='.$pages).'">'.$pages.'</a>';
		}	
		//加入下一页箭头
		if($page<=$pages-1)
		{
			$right .='<a href="'.site_url($url.'page='.($page+1)).'">&raquo;</a>';
		}
		
		$page_con .= $right;
		
		return $page_con;
	}
}

/* End of file pager_helper.php */
/* Location: ./application/helpers/pager_helper.php */