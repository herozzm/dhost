<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>编辑站点</title>
<link href="/skin/my/style/main.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://libs.baidu.com/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#tags>a').click(function() {
        $(this).parent().children().removeClass();
        $('.addmain').hide().eq($(this).addClass('cur').index()).show();
    });


    $("select[name!=is_visit]").change(function() {
        if ($(this).val() == '0')
        {
            $(this).parents('table').find('input').attr('disabled', 'disabled');
        } else {
            $(this).parents('table').find('input').removeAttr('disabled');
        }

    })

    $("select[name!=is_visit]").each(function() {
        if ($(this).val() == '0')
        {
            $(this).parents('table').find('input').attr('disabled', 'disabled');
        } else {
            $(this).parents('table').find('input').removeAttr('disabled');
        }

    })

    //生成md5密钥
    $('.autopwd').click(function() {
        var target_input = $(this).prev('input')
        var val_input = $(this).parents('tr').prev().find('input');
        var md5_16 = val_input.val();
        var md5_type = $(this).attr('title');
        if (md5_16 != '')
        {
            $.post('<?=site_url('main/ajax_md5')?>', {
                md5_type: md5_type,
                md5_16: md5_16
            },
            function(data) {
                target_input.val(data);
            })

        } else {
            alert('是根据' + md5_type + '用户名自动生成，请先填写' + md5_type + '账户信息');
            val_input.focus();
        }
    });

    //生成临时域名
    $('#autodomain').click(function() {
        var target_input = $(this).prev('input');
        var ranDate = new Date();
        $.post('<?=site_url('main/ajax_domain')?>?t=' + ranDate.getTime(),
        function(data) {
            target_input.val(data);
        })

    });

    //删除mysql数据库确认
    $('#is_mysql').change(function() {
        if ($(this).val() == '0')
        {
            id = $('#s_id').val();
            if (id)
            {
                var ranDate = new Date();
                $.post('<?=site_url('main/ajax_mysql')?>?t=' + ranDate.getTime(), {
                    id: id
                },
                function(data) {
                    if (data == 1)
                    {
                        var msg = "设置成“不开通”将在提交后彻底删除数据库\n\n确定删除请输入“删除”，不删除点击取消或关闭对话框";
                        if (prompt(msg) == '删除') {
                            $('#is_mysql').val('0');

                        } else {
                            $('#is_mysql').val('1');
                            $('#is_mysql').parents('table').find('input').removeAttr('disabled');
                        }
                    }
                })
            }
        }
    });
});


function check()
 {
    var put1 = $("#mainform input[name='uname']");
    var put2 = $("#mainform input[name='domain']");
    var put3 = $("#mainform input[name='dir']");

    var sel1 = $("#mainform select[name='is_ftp']");
    var sel2 = $("#mainform select[name='is_mysql']");

    if (put1.val() == '')
    {
        alert('客户名称不能为空');
        put1.focus();
        return false

    }
    if (put2.val() == '')
    {
        alert('绑定域名不能为空');
        put2.focus();
        return false

    }
    if (put3.val() == '')
    {
        alert('对应目录不能为空');
        put3.focus();
        return false

    }
    if (sel1.val() == '1')
    {
        var put4 = $("#mainform input[name='ftp_user']");
        var put5 = $("#mainform input[name='ftp_pwd']");
        if (put4.val() == '' || put5.val() == '')
        {
            alert('开通FTP，FTP账户和密码必填');
            return false
        }
    }

    if (sel2.val() == '1')
    {
        var put6 = $("#mainform input[name='mysql_base']");
        var put7 = $("#mainform input[name='mysql_user']");
        var put8 = $("#mainform input[name='mysql_pwd']");
        if (put6.val() == '' || put7.val() == '' || put8.val() == '')
        {
            alert('开通MYSQL，数据库名称、mysql账户、mysql密码必填');
            return false
        }
		if(put7.val().length>16)
		{
			alert('mysql账户不能超过16位');
			put7.focus();
			return false
		}
    }

	//通过验证，提交按钮设置成禁用，防止重复提交
	$('#subtn').attr('disabled', 'disabled');
}
</script>
</head>

<body>
<div class="wrap">
  <div class="navtag"><span><a href="<?=site_url('home/out')?>">退出登录</a></span><a href="<?=site_url('main')?>" class="cur">站点列表</a></div>
  <div class="search"><span><a href="<?=site_url('main')?>"> + 站点列表</a></span>
    <form action="<?=site_url('main')?>" method="get">
      <input type="text" name="keys" class="input1" placeholder="客户名称、域名、目录等" />
      <select name="is_visit">
        <option value="">不限能否访问</option>
        <option value="1"<? if($is_visit==='1') {?> selected="selected"<? }?>>已开通访问</option>
        <option value="0"<? if($is_visit==='0') {?> selected="selected"<? }?>>没开通访问</option>
      </select>
      <select name="is_ftp">
        <option value="">不限ftp</option>
        <option value="1"<? if($is_ftp==='1') {?> selected="selected"<? }?>>已开通ftp</option>
        <option value="0"<? if($is_ftp==='0') {?> selected="selected"<? }?>>没开通ftp</option>
      </select>
      <select name="is_mysql">
        <option value="">不限mysql</option>
        <option value="1"<? if($is_mysql==='1') {?> selected="selected"<? }?>>已开通mysql</option>
        <option value="0"<? if($is_mysql==='0') {?> selected="selected"<? }?>>没开通mysql</option>
      </select>
      <input type="submit" value="" class="s_ico" />
      <?=!empty($keys) ? '<a href="'.site_url('main').'"> &laquo;返回所有站点</a>' : ''?>
    </form>
  </div>
  <div class="addwrap">
    <form action="<?=empty($res) ? site_url('main/ok/add') : site_url('main/ok/edit') ?>" method="post" id="mainform" onsubmit="return check()">
    <div class="tags" id="tags"><a href="javascript:void(0);" class="cur">基本信息</a><a href="javascript:void(0);">FTP</a><a href="javascript:void(0);">MYSQL</a></div>
    <table class="addmain">
      <tr>
        <td class="tright">是否访问</td>
        <td>
        <?
        if(!empty($id))
		{
		?>
        <input name="s_id" value="<?=$id?>" type="hidden" id="s_id" />
        <?
		}
		?>
        <select name="is_visit">
            <option value="1"<? if(!empty($res['is_visit'])) {?> selected="selected"<? }?>>开通访问</option>
            <option value="0"<? if(empty($res['is_visit']) && !empty($res)) {?> selected="selected"<? }?>>不开通访问</option>
          </select>
        </td>
      </tr>
      <tr>
        <td width="270" class="tright">客户名称</td>
        <td><input type="text" name="uname" class="input1" value="<?=$res['uname']?>" />
        *</td>
      </tr>
      <tr>
        <td class="tright">绑定域名</td>
        <td><input type="text" name="domain" class="input1" value="<?=$res['domain']?><?=$res['ports']>80 ? ':'.$res['ports'] : ''?>" />
        * <a href="javascript:void(0);" id="autodomain">自动生成临时域名</a>(多域名半角逗号,分开)</td>
      </tr>
      <tr>
        <td class="tright">对应目录</td>
        <td>
        <?
        if(empty($res))
		{
		?>
        <input type="text" name="dir" class="input1" value="<?=$res['dir']?>" />
        <?
		} else {
		?>
        <?=$res['dir']?>
        <?
		}
		?>
        *
        </td>
      </tr>
    </table>
    <table class="addmain" style="display: none">
      <tr>
        <td width="270" class="tright">是否开通</td>
        <td><select name="is_ftp">
            <option value="0"<? if(empty($res['is_ftp'])) {?> selected="selected"<? }?>>不开通FTP</option>
            <option value="1"<? if(!empty($res['is_ftp'])) {?> selected="selected"<? }?>>开通FTP</option>
          </select>
          (不开通的话可以通过全局FTP用户管理)</td>
      </tr>
      <tr>
        <td class="tright">FTP账户</td>
        <td><input type="text" name="ftp_user" class="input1" value="<?=$res['ftp_user']?>" /></td>
      </tr>
      <tr>
        <td class="tright">FTP密码</td>
        <td><input type="text" name="ftp_pwd" class="input1" value="<?=$res['ftp_pwd']?>" /> <a href="javascript:void(0);" class="autopwd" title="ftp">自动生成</a></td>
      </tr>
    </table>
    <table class="addmain" style="display: none">
      <tr>
        <td width="270" class="tright">是否开通</td>
        <td><select name="is_mysql" id="is_mysql">
            <option value="0"<? if(empty($res['is_mysql'])) {?> selected="selected"<? }?>>不开通MYSQL</option>
            <option value="1"<? if(!empty($res['is_mysql'])) {?> selected="selected"<? }?>>开通MYSQL</option>
          </select>
          <? if(!empty($res['is_mysql'])) {?>
          (如果之前有开通过该站点数据，设置成“不开通”将删除数据库，建议先备份!)
          <? }?>
          </td>
      </tr>
      <tr>
        <td class="tright">数据库名称</td>
        <td>
        <?
        if(empty($res))
		{
		?>
        <input type="text" class="input1" name="mysql_base" value="<?=$res['mysql_base']?>" />
        <?
		} else {
		  if(empty($res['mysql_base']))
		  {
		?>
        <input type="text" class="input1" name="mysql_base" value="<?=$res['mysql_base']?>" />
        <?
		  } else {
		    echo $res['mysql_base'];
		  }
		}
		?>
        </td>
      </tr>
      <tr>
        <td class="tright">mysql账户</td>
        <td>
        <?
        if(empty($res))
		{
		?>
        <input type="text" name="mysql_user" class="input1" value="<?=$res['mysql_user']?>" />
        <?
		} else {
		  if(empty($res['mysql_user']))
		  {
		   echo $res['mysql_user'];
		?>
        <input type="text" class="input1" name="mysql_user" value="<?=$res['mysql_user']?>" />
        <?
		  } else {
		    echo $res['mysql_user'];
		  }
		}
		?>不能是纯数字、不能超过16个字符
        </td>
      </tr>
      <tr>
        <td class="tright">mysql密码</td>
        <td>
        <input type="text" name="mysql_pwd" class="input1" value="<?=$res['mysql_pwd']?>" /> <a href="javascript:void(0);" class="autopwd" title="mysql">自动生成</a>
        </td>
      </tr>
    </table>
    <div class="btnwrap">
      <input type="submit" class="subtn" value="提 交" id="subtn" />
    </div>
    </form>
  </div>

</div>
</body>
</html>
