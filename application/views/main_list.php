<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>站点列表</title>
<link href="/skin/my/style/main.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://libs.baidu.com/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.tlist tr:even').addClass('tr1');
	$('.tlist tr').hover(function(){
	  $(this).addClass('tr2');
	  $(this).children('td:first').children('span').show();
	},function(){
	  $(this).removeClass('tr2');
	  $(this).children('td:first').children('span').hide();
	})

	$('.del').click(function(){
      $(this).parents('tr').addClass('tr3');

	  var msg = "将彻底删除该站点代码和数据\n\n删除请输入“删除”，不删除点击取消";
	  if (prompt(msg)=='删除'){
		return true;
	  }else{
	    $(this).parents('tr').removeClass('tr3');
		return false;
	  }
	})
});
</script>
</head>

<body>
<div class="wrap">
  <div class="navtag"><span><a href="<?=site_url('home/out')?>">退出登录</a></span><a href="<?=site_url('main')?>" class="cur">站点列表</a></div>
  <div class="search"><span><a href="<?=site_url('main/add')?>"> + 增加站点</a></span>
    <form action="<?=site_url('main')?>" method="get">
      <input type="text" name="keys" class="input1" placeholder="客户名称、域名、目录等" />
      <select name="is_visit">
        <option value="">不限能否访问</option>
        <option value="1"<? if($is_visit==='1') {?> selected="selected"<? }?>>已开通访问</option>
        <option value="0"<? if($is_visit==='0') {?> selected="selected"<? }?>>没开通访问</option>
      </select>
      <select name="is_ftp">
        <option value="">不限ftp</option>
        <option value="1"<? if($is_ftp==='1') {?> selected="selected"<? }?>>已开通ftp</option>
        <option value="0"<? if($is_ftp==='0') {?> selected="selected"<? }?>>没开通ftp</option>
      </select>
      <select name="is_mysql">
        <option value="">不限mysql</option>
        <option value="1"<? if($is_mysql==='1') {?> selected="selected"<? }?>>已开通mysql</option>
        <option value="0"<? if($is_mysql==='0') {?> selected="selected"<? }?>>没开通mysql</option>
      </select>
      <input type="submit" value="" class="s_ico" />
      <?=!empty($keys) ? '<a href="'.site_url('main').'"> &laquo;返回所有站点</a>' : ''?>
    </form>
  </div>
  <?
	if(!empty($list))
	{
  ?>
  <table class="tlist">
    <tr>
      <th width="22%">客户名称</th>
      <th width="13%">绑定域名</th>
      <th width="10%">目录</th>
      <th width="15%">FTP信息</th>
      <th width="15%">数据库</th>
      <th width="15%">开通/修改时间</th>
      <th width="10%">站点状态</th>
    </tr>
    <?
	  foreach($list as $v)
	  {
		  $status   = empty($v['status']) ? '<span style="text-decoration: line-through; color: #ccc">已删除</span>' : '正常';
		  $is_visit = (empty($v['is_visit']) && !empty($v['status'])) ? '<br /><span style=" color:#c00">未开通访问</span>' : '';
		  $edittime = empty($v['edittime']) ? '' : '<br />'.date('Y-m-d H:i:s', $v['edittime']);
	?>
    <tr>
      <td class="uname">
        <?
        if(!empty($v['status']))
		{
		?>
        <span><a href="<?=site_url('main/edit/'.$v['s_id'])?>">修改</a> <a href="<?=site_url('main/ok/del/?id='.$v['s_id'])?>" class="del">删除</a></span>
        <?
		}
		?>
		<?=$v['uname']?></td>
      <td><a href="http://<?=reset(explode(',',$v['domain']))?><?=$v['ports']>80 ? ':'.$v['ports'] : ''?>" target="_blank"><?=$v['domain']?><?=$v['ports']>80 ? ':'.$v['ports'] : ''?></a></td>
      <td><?=$v['dir']?></td>
      <td><? if($v['is_ftp']) {?><?=$v['ftp_user']?><br /><?=$v['ftp_pwd']?><? } else {?>未开通<? }?></td>
      <td><? if($v['is_mysql']) {?><?=$v['mysql_user']?><br /><?=$v['mysql_pwd']?><? } else {?>未开通<? }?></td>
      <td><?=date('Y-m-d H:i:s', $v['addtime']), $edittime?></td>
      <td class="f1"><?=$status,$is_visit?></td>
    </tr>
    <?
	  }
	?>
  </table>
  <div class="pager"><?=$pager?></div>
  <?
	}
  ?>
</div>
</body>
</html>