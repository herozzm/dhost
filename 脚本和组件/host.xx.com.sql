/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50532
Source Host           : localhost:3306
Source Database       : host.xx.com

Target Server Type    : MYSQL
Target Server Version : 50532
File Encoding         : 65001

Date: 2014-10-15 19:03:33
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `dc_admin`
-- ----------------------------
DROP TABLE IF EXISTS `dc_admin`;
CREATE TABLE `dc_admin` (
  `u_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uname` varchar(30) NOT NULL DEFAULT '',
  `pwd` varchar(16) NOT NULL DEFAULT '' COMMENT 'md5 16位加密+混淆字符串',
  `flag` varchar(4) NOT NULL DEFAULT '1111' COMMENT '查增改删 4位设置，0无权限，1有权限,默认1111超级管理员',
  PRIMARY KEY (`u_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dc_admin
-- ----------------------------
INSERT INTO `dc_admin` VALUES ('1', 'admin', 'admin', '1111');

-- ----------------------------
-- Table structure for `dc_log`
-- ----------------------------
DROP TABLE IF EXISTS `dc_log`;
CREATE TABLE `dc_log` (
  `l_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `s_id` int(10) unsigned NOT NULL COMMENT '站点ID',
  `u_id` int(10) unsigned NOT NULL COMMENT '操作员ID',
  `action` text NOT NULL COMMENT '操作内容',
  PRIMARY KEY (`l_id`),
  KEY `s_id` (`s_id`),
  KEY `u_id` (`u_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dc_log
-- ----------------------------

-- ----------------------------
-- Table structure for `dc_site`
-- ----------------------------
DROP TABLE IF EXISTS `dc_site`;
CREATE TABLE `dc_site` (
  `s_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `uname` varchar(50) NOT NULL DEFAULT '',
  `is_visit` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `domain` varchar(100) NOT NULL DEFAULT '',
  `ports` int(5) unsigned NOT NULL DEFAULT '80',
  `dir` varchar(100) NOT NULL,
  `is_ftp` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ftp_user` varchar(50) NOT NULL DEFAULT '',
  `ftp_pwd` varchar(50) NOT NULL DEFAULT '',
  `is_mysql` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `mysql_user` varchar(50) NOT NULL DEFAULT '',
  `mysql_pwd` varchar(50) NOT NULL DEFAULT '',
  `mysql_base` varchar(50) NOT NULL DEFAULT '',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`s_id`),
  KEY `status` (`status`),
  KEY `uname` (`uname`),
  KEY `domain` (`domain`),
  KEY `is_ftp` (`is_ftp`),
  KEY `is_mysql` (`is_mysql`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dc_site
-- ----------------------------
